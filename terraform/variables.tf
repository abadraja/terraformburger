# =============== Public Key =============== #
variable "PATH_TO_PUBLIC_KEY" {
  default = ""
}

# ============= AWS access key ============= #

# ============= AWS secret key ============= #

# =============== Aws Region =============== #

# ============== Instance Name ============= #

variable "AMIS" {
  type = "map"

  default = {
    us-east-1 = "ami-13be557e"
    us-west-2 = "ami-06b94666"
    eu-west-1 = "ami-844e0bf7"
  }
}

data "aws_vpc" "default" {
  default = true
}
